
#ifndef SINGLETON_H
#define SINGLETON_H

template<typename T>
//T is semiregular or regular or totally ordered
struct singleton
{
	typedef T value_type;
	T value;

	//Conversion - Implicit conversion is only for 2 layers i.e. a->b, b->c where a to b can be user-defined
	//Eg. class A -> int possible, double(A)
	//Explicit conversion can be implicit in while, if ...
	//Eg. cin -> (void*)NULL . This pointer can be converted to bool. 0<<42 eg. while(cin) is implicit

	//Conversion from T and to T
	explicit singleton(const T& x):value(x) {}
	explicit operator T() const { return value; }

	template<typename U>
	singleton(const singleton<U>& x) :value(x.value) {}

	//semiregular (No equality)
	singleton(const singleton &x):value(x.value) {}			//Could be implicitly declared
	//default constructor
	singleton() {}							//Could be implicitly declared sometimes
	~singleton() {}							//Could be implicitly declared
	singleton &operator=(const singleton& x){			//Could be implicitly declared
		value = x.value;
		return *this;
	}



	//regular (Equality)
	friend
	bool operator==(const singleton& x, const singleton& y){
		return x.value == y.value;
	}

	friend
	bool operator!=(const singleton& x, const singleton& y){
		return !(x==y);
	}

	//totally-ordered (Ordered set)
	friend
	bool operator<(const singleton& x, const singleton& y){
		return (x.value < y.value);
	}

	friend
	bool operator>(const singleton& x, const singleton& y){
		return y<x;
	}

	friend
	bool operator<=(const singleton& x, const singleton& y){
		return !(y<x);
	}

	friend
	bool operator>=(const singleton& x, const singleton& y){
		return !(x<y);
	}

	void setDevicedata(const singleton& x){
		value = x.value;
	}

};

#endif

